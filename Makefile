

GOPATH:=$(shell go env GOPATH):$(shell pwd)

all:
	GOPATH=${GOPATH} go install android_test

clean:
	-rm -rf bin

.PHONY: all clean
