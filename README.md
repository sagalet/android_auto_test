Android Auto Retry
===================
This is for android auto testing. It will auto retry and summary how much time for each run.<br />
Include cts, gts, vts,and sts.

Requiretment
============

<h5>1. golang - Please Download from https://golang.org/</h5>


<h5>2. Install the requirement packages</h5>


go get github.com/kr/pty<br />
go get golang.org/x/crypto/ssh/terminal

Build
======

<h5>Make all</h5>
The executable app will be in ./bin/

Usage
=====

<h5>./android_test [retry] [app] [command]</h5>
retry : the times to retry or skip it if you don't want. ex: ./android_test 2 , the program will executed first and retry 2 times<br />
app : the test you want to execute. ex: ~/android-cts/tool/cts-tradefed<br />
command : the plan you want to execute. ex : run cts --shards 4 --exclude-filter .......<br />

<h5>ex:</h5>
./android_test 2 ~/android-cts/tool/cts-tradefed run cts .......
