package precondition

import (
	"adb"
	"bytes"
	"common"
	"errors"
	"fmt"
	"strings"
)

func checkAdb() error {
	out := common.GetSingleCommandResult("adb version")
	if out.Len() == 0 {
		return errors.New("Can not find adb\nPlease download platform-tool from\nhttps://developer.android.com/studio/releases/platform-tools\n\n")
	}
	return nil
}

func checkPython() error {
	// list of required packages
	list := []string{
		"python-dev",
		"python-protobuf",
		"protobuf-compiler",
		"python-virtualenv",
		"python-pip",
	}

	failed := []string{}

	// check python
	out := common.GetSingleCommandResult("which python")
	if out.Len() == 0 {
		failed = append(failed, "python")
	}

	// check package
	for _, v := range list {
		var buf bytes.Buffer
		buf.WriteString("grep ")
		buf.WriteString(v)
		out = common.RunPipeCommands("dpkg --get-selections", buf.String())
		if out.Len() == 0 {
			failed = append(failed, v)
		}
	}

	var err error = nil

	// generate error message if any package missed
	if len(failed) != 0 {
		message := strings.Join(failed, " ")
		err = errors.New(fmt.Sprintf("missed : %s\nInstall the missed packages :\nsudo apt-get install -y %s\n\n", message, message))
	}
	return err
}

// check adb, python, devices
func Precondition() error {
	var failed bool = false

	// check adb
	err := checkAdb()
	if err != nil {
		fmt.Printf("%s\n", err)
		failed = true
	}

	// check python
	err = checkPython()
	if err != nil {
		fmt.Printf("%s\n", err)
		failed = true
	}

	// check connected device
	devices := adb.GetDeviceList()
	if len(devices) == 0 {
		fmt.Println("No device connected!!")
		failed = true
	}

	if failed == true {
		return errors.New("precondition failed\n")
	}
	return nil
}
