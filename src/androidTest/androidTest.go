package androidTest

import (
	"adb"
	"bytes"
	"common"
	"fmt"
	"github.com/kr/pty"
	"golang.org/x/crypto/ssh/terminal"
	"io"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"time"
)

type RunCommand struct {
	Retry  int
	App    string
	Plan   []string
	Param  []string
	Shards []string
}

func ShowCommand(run *RunCommand) {
	fmt.Printf("\033[96mcmd    : \t\033[92m%s\033[0m\n", run.App)
	fmt.Printf("\033[96mplan   : \t\033[92m%s %s\033[0m\n", run.Plan[0], run.Plan[1])
	fmt.Printf("\033[96mparam  : \t\033[92m%v\033[0m\n", run.Param)
	fmt.Printf("\033[96mretry  : \t\033[92m%d\033[0m\n", run.Retry)
	fmt.Printf("\033[96mshards : \t\033[92m%v\033[0m\n", run.Shards)
}

func showSimpleCommand(run *RunCommand) string {
	return fmt.Sprintf("%s %s", run.App, strings.Join(append(run.Plan, run.Param...), " "))
}

func runTest(run *RunCommand) {
	var cmd *exec.Cmd
	fmt.Printf("\033[33m\nrun ===> %s\033[0m \n\n", showSimpleCommand(run))
	cmd = exec.Command(run.App, append(run.Plan, run.Param...)...)
	p, err := pty.Start(cmd)
	if err != nil {
		fmt.Println("start pty failed ", err)
	}
	defer p.Close()
	oldState, err := terminal.MakeRaw(int(os.Stdin.Fd()))
	if err != nil {
		panic(err)
	}
	defer func() { _ = terminal.Restore(int(os.Stdin.Fd()), oldState) }() // Best effort.

	go func() {
		time.Sleep(4 * time.Second)
		io.WriteString(p, "exit\n")
		io.Copy(p, os.Stdin)
	}()

	_, err = io.Copy(os.Stdout, p)
}

func retryTest(run *RunCommand) {
	session := getLastSession(run)
	run.Param = append([]string{"--retry", strconv.Itoa(session)}, run.Shards...)
	app := strings.Split(run.App, "/")
	if strings.Compare(app[len(app)-1], "gts-tradefed") == 0 { //this is gts , we have to use "run retry" instead
		run.Plan[1] = "retry"
	}
	runTest(run)
	run.Retry--
}

func getLastSession(run *RunCommand) int {
	var buf bytes.Buffer
	buf.WriteString(run.App)
	buf.WriteString(" ")
	buf.WriteString("list r")
	out := common.RunPipeCommands(buf.String(), "tail -n 3", "head -n 1")
	session, err := strconv.ParseInt(strings.Split(out.String(), " ")[0], 10, 32)
	if err != nil {
		fmt.Println("get last session failed ", err)
		session = -1
	}
	return int(session)
}

func format(cmd string, start time.Time, end time.Time) string {
	var buf bytes.Buffer
	buf.WriteString(fmt.Sprintf("\033[96mcmd  : \033[92m%s\033[0m\n", cmd))
	buf.WriteString(fmt.Sprintf("\033[96mtime : \033[92m%s - %s (%v)\033[0m\n", start.Format("2006-01-02 15:04:05"), end.Format("2006-01-02 15:04:05"), end.Sub(start)))
	return buf.String()
}

func StartExecute(run *RunCommand) {

	var logs []string = make([]string, 0)
	var start, end time.Time

	// first run
	start = time.Now()
	runTest(run)
	end = time.Now()
	logs = append(logs, format(showSimpleCommand(run), start, end))

	// check if need to retry
	for run.Retry > 0 {
		//reboot all devices
		adb.RebootAllDevice()
		start = time.Now()
		retryTest(run)
		end = time.Now()
		logs = append(logs, format(showSimpleCommand(run), start, end))
	}

	fmt.Println("\n\n\033[30;47mSummary:\033[0m")
	for _, s := range logs {
		fmt.Println(s)
	}
}
