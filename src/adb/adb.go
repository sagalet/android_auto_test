package adb

import (
	"common"
	"fmt"
	"strings"
	"sync"
	"time"
)

func GetDeviceList() []string {
	out := common.GetSingleCommandResult("adb devices")
	str := strings.Split(out.String(), "\n")[1:]
	devices := []string{}
	for _, v := range str {
		if strings.Contains(v, "device") {
			devices = append(devices, strings.Split(v, "\t")[0])
		}
	}
	return devices
}

func rebootSingleDeviceThreaded(device string, wait *sync.WaitGroup) {
	common.RunPipeCommands(fmt.Sprintf("adb -s %s reboot", device), "adb wait-for-device")
	time.Sleep(10 * time.Second)
	wait.Done()
	fmt.Printf("%s reboot!!!\n", device)
}

func RebootAllDevice() {
	devices := GetDeviceList()
	if len(devices) == 0 {
		return
	}

	var wait sync.WaitGroup
	wait.Add(len(devices))
	for _, d := range devices {
		go rebootSingleDeviceThreaded(d, &wait)
	}
	wait.Wait()
	fmt.Println("reboot all finished")
}
