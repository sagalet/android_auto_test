package main

import (
	"androidTest"
	"errors"
	"fmt"
	"os"
	"precondition"
	"strconv"
	"strings"
)

const (
	INIT   = iota
	RETRY  = iota
	APP    = iota
	SHARDS = iota
	PLAN   = iota
)

func tryParseParam() (*androidTest.RunCommand, error) {

	//run cts as default
	cmd := androidTest.RunCommand{
		Retry: 0,
		App:   "./cts-tradefed",
		Plan:  []string{"run", "cts"},
	}

	//No any arg, run cts
	if len(os.Args) == 1 {
		return &cmd, nil
	}

	var index int = 1
	state := INIT

	for index < len(os.Args) {
		switch state {
		case INIT: //parse retry
			//try parse retry
			if retry, err := strconv.ParseInt(os.Args[index], 10, 32); err != nil {
				cmd.Retry = 0
			} else {
				cmd.Retry = int(retry)
				index++
			}
			state = RETRY
		case RETRY: //parse app , cts vts or ......
			app := strings.Split(os.Args[index], "/")
			switch app[len(app)-1] {
			case "cts-tradefed", "vts-tradefed", "gts-tradefed", "sts-tradefed":
				cmd.App = os.Args[index]
				state = APP
				index++
			default:
				return nil, errors.New(fmt.Sprintf("parse test item failed. %s\n", os.Args[index]))
			}
		case APP: //parse test plan , ex : run xxx
			if strings.Compare(os.Args[index], "run") != 0 {
				return nil, errors.New(fmt.Sprintf("the first command must be \"run\" : %s\n", strings.Join(os.Args[index:], " ")))
			}
			if index+1 >= len(os.Args) {
				return nil, errors.New(fmt.Sprintf("There is no test plan after \"run\": %s\n", strings.Join(os.Args[index:], " ")))
			}
			cmd.Plan = os.Args[index : index+2]
			index += 2
			state = PLAN
		case PLAN: //parse shards
			for k, v := range os.Args[index:] {
				if strings.Compare(v, "--shards") == 0 || strings.Compare(v, "--shard-count") == 0 {
					next := k + index + 1
					if next > len(os.Args) {
						break
					}
					cmd.Shards = make([]string, len(os.Args[k+index:next+1]))
					copy(cmd.Shards, os.Args[k+index:next+1])
				}
			}
			state = SHARDS
		case SHARDS: //parse parameter, ex : --exclude-filter --shards .....
			cmd.Param = make([]string, len(os.Args[index:]))
			copy(cmd.Param, os.Args[index:])
			index += len(os.Args[index:])

		}
	}

	return &cmd, nil
}

func main() {

	// check adb, python, devices
	err := precondition.Precondition()
	if err != nil {
		return
	}

	// check parameter
	param, err := tryParseParam()
	if err != nil {
		fmt.Printf("%s\n", err)
		return
	}

	// check test tool
	if _, err := os.Stat(param.App); os.IsNotExist(err) {
		fmt.Printf("%s : no such file\n", param.App)
		return
	}

	androidTest.ShowCommand(param)
	androidTest.StartExecute(param)
}
