package common

import (
	"bytes"
	"os/exec"
	"strings"
)

func GetSingleCommandResult(str string, in ...bytes.Buffer) bytes.Buffer {
	tmp := strings.Split(str, " ")
	var cmd *exec.Cmd
	var out bytes.Buffer
	if len(tmp) == 1 {
		cmd = exec.Command(tmp[0])
	} else {
		cmd = exec.Command(tmp[0], tmp[1:]...)
	}
	if len(in) != 0 {
		cmd.Stdin = &in[0]
	}
	cmd.Stdout = &out
	cmd.Run()
	return out
}

func RunPipeCommands(str ...string) bytes.Buffer {
	var out bytes.Buffer
	for _, v := range str {
		out = GetSingleCommandResult(v, out)
	}
	return out
}
